
let userNumber = +prompt("Please enter number");
while (isNaN(userNumber) || userNumber == "" || !Number.isInteger(userNumber)) {
  userNumber = +prompt("Enter correct number")
};

const x = 5

if (userNumber >= x) {
  for (let i = 0; i <= userNumber; i++) {
    if (i % x === 0 && i !== 0) {
      console.log(i);
    }
  }
} else {
  console.log("Sorry, no numbers");
};

